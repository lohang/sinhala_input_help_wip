---

name: "New Data"
about: "This template is for adding new data to the repository"
title: "[NEW DATA] "

---
- මෙහෙයුම් පද්ධතිය කුමක් ද?


- මෙහෙයුම් පද්ධතියේ සංස්කරණ අංකය (version number) කුමක් ද?


- මෙහෙයුම් පද්ධතියේ සංස්කරණ නාමය (version name) කුමක් ද? (අනිවාර්ය නැත)


- Desktop environment එක කුමක් ද?


- ස්ථාපනය කළ යුතු පැකේජ මොනවා ද?


- අමතර සටහන්, විශේෂ උපදෙස්, ගැටලු තිබේනම් පහත කොටසෙහි සඳහන් කරන්න.
=====

=====