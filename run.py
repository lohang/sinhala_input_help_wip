from os import path, makedirs, walk, unlink, listdir
from shutil import copyfile, rmtree
import toml
from jinja2 import Environment, FileSystemLoader
from htmlmin.minify import html_minify

workingDir = path.dirname(path.realpath(__file__))
dataDir = path.join(workingDir, "data")
outputDir = path.join(workingDir, "output")
templatesDir = path.join(workingDir, "templates")

def makeCleanFolder(folderPath):
    if path.exists(folderPath):
        for root, dirs, files in walk(folderPath):
            for filePath in files:
                unlink(path.join(root, filePath))
            for dirPath in dirs:
                rmtree(path.join(root, dirPath))
    else:
        makedirs(folderPath)

makeCleanFolder(outputDir)
copyfile(path.join(workingDir, "simple.min.css"), path.join(outputDir, "simple.min.css"))

env = Environment(loader=FileSystemLoader(templatesDir), keep_trailing_newline=True)

distros = {}
for distroCode in listdir(dataDir):
    distroDir = path.join(dataDir, distroCode)
    distros[distroCode] = toml.load(path.join(distroDir, "info.toml"))
    
    distros[distroCode]['versions'] = {}
    for versionFileName in listdir(distroDir):
        if versionFileName == "info.toml": continue
        distros[distroCode]['versions'][versionFileName.replace(".toml", "")] = toml.load(path.join(distroDir, versionFileName))

    

print(distros)


with open(path.join(outputDir, "index.html"), "w") as writeFile:
    html = env.get_template('home.html').render(distros=distros)
    writeFile.write(html_minify(html))

for distroCode, distroInfo in distros.items():
    for versionCode, versionInfo in distroInfo['versions'].items():
        with open(path.join(outputDir, f'{distroCode}_{versionCode}.html'), "w") as writeFile:
            html = env.get_template('distro.html').render(distroInfo=distroInfo, versionInfo=versionInfo)
            writeFile.write(html_minify(html))
